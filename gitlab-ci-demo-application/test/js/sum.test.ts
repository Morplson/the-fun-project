import sum from "../../src/js/sum"

describe('sum(a, b) = c: The sum of a and b should be c', () => {
  test('sum(1, 1) = 2: 1 + 1 = 2', () => {
    // arange
    const a = 1;
    const b = 1;
    const expectedResult = 2;

    // act
    const result = sum(a, b);

    // assert
    expect(result).toBe(expectedResult);
  });

  test('sum(1, 2) = 3: 1 + 2 = 3', () => {
    // arange
    const a = 1;
    const b = 2;
    const expectedResult = 3;

    // act
    const result = sum(a, b);

    // assert
    expect(result).toBe(expectedResult);
  });
});
